import { NgModule } from '@angular/core';
import { SociabiliComponent } from './sociabili.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const routes = [
  {
    path: 'sociabili', component: SociabiliComponent
  }
];
@NgModule({
  declarations: [SociabiliComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class SociabiliModule { }
