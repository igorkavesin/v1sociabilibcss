import { Component } from '@angular/core';
import { SharedConfig } from '../shared/shared.config';

@Component({
  selector: 'app-sociabili',
  templateUrl: './sociabili.component.html'
})
export class SociabiliComponent { 

  title = '';
  constructor( public config: SharedConfig) {
    this.title = config.prefix;
  }
}
