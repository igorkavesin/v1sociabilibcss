import { BrowserModule } from '@angular/platform-browser';
import { NgModule, COMPILER_OPTIONS } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from '../app/shared/shared.module';
import { SharedConfig } from './shared/shared.config';
import { RouterModule } from '@angular/router';
import { SociabiliModule } from './sociabili/sociabili.module';
import { BcssModule } from './bcss/bcss.module';
import { AppRoutingModule } from './app-routing.module';

const routes = [
  {
    path: '', redirectTo: 'sociabili', pathMatch: 'full'
  },
  {
    path: 'bcss',
    loadChildren: () => import('../app/bcss/bcss.module').then(m => m.BcssModule)
  }
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    SharedModule,
    BrowserModule,
    SociabiliModule,
    BcssModule,
    AppRoutingModule
  ],
  providers: [{
    provide: SharedConfig, useValue: {
        prefix: 'PROJECT SOCIABILI AND BCSS...'
    }
}],
  bootstrap: [AppComponent]
})
export class AppModule { }
