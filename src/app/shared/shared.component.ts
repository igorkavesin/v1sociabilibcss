import { NgModule, Component, Input} from '@angular/core';
import { SharedConfig } from './shared.config';

@Component({
    selector: 'app-shared',
template: `
    <h1>It\'s works component was shared!</h1>
    {{prefix}} <br>
    {{labelProject}} `,
    styles: [':host {display:block; background:green; padding: 10px;}']

})
export class SharedComponent {
    @Input() public labelProject: string;
    prefix: string;
    constructor( config: SharedConfig) {
        this.prefix = config.prefix;
    }
}
