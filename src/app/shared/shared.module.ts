import { NgModule, ModuleWithProviders } from '@angular/core';
import { SharedComponent } from './shared.component';
import { SharedConfig } from './shared.config';

@NgModule({
    declarations: [SharedComponent],
    exports: [SharedComponent],
    providers: [{
        provide: SharedModule, useValue: {
            prefix: 'Default Prefix'
        }
    }]
})

export class SharedModule {
    static forRoot(config: SharedConfig): ModuleWithProviders {
        
        return {
            ngModule: SharedModule,
            providers: [{
                provide: SharedModule, useValue: config
            }]
        };
    }
}
declare module '@angular/core' {
    interface ModuleWithProviders<T = any> {
        ngModule: Type<T>;
        providers?: Provider[];
    }
  }
