import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BcssComponent } from './bcss.component';

const routes = [
  {
    path: 'bcss', component: BcssComponent
  }
];
@NgModule({
  declarations: [BcssComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class BcssModule { }
